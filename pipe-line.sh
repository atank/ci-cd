#token generatiob

access_token=$(curl -s -k  -X 'POST' -H 'Host: platform.checkmarx.net' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Content-Length: 168' --data-binary 'username=dummy%40mobiquityinc.com&password=dummy123&acr_values=tenant%3Amobiquity&scope=sca_api&client_id=sca_resource_owner&grant_type=password' 'https://platform.checkmarx.net/identity/connect/token' | jq -r .access_token)

echo "this is access token "$access_token

#project creation and team name, use it only for project generation after that this value can be hard coded

#project_id=$(curl -s -k  -X $'POST'  -H $'Host: api-sca.checkmarx.net' -H $'Accept-Encoding: gzip, deflate' -H $'Authorization: Bearer '$access_token -H $'Accept: */*' -H $'Accept-Language: en' -H $'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36' -H $'Content-Type: application/json' --data-binary $'{ \"name\": \"checksca77\", \"assignedTeams\": [\"/CxServer\"]}' $'https://api-sca.checkmarx.net/risk-management/projects' | jq -r .id)

project_id="16d8a768-1c3a-4f4f-9360-dfae6f9e1c93"

echo "This is projectid "$project_id

#upload URL generation

upload_url=$(curl -s -k -X $'POST'  -H $'Host: api-sca.checkmarx.net' -H $'Accept-Encoding: gzip, deflate' -H $'Authorization: Bearer '$access_token -H $'Accept: */*' -H $'Accept-Language: en' -H $'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36' -H $'Content-Type: application/json' -H $'Content-Length: 52' --data-binary $'{ \"name\": \"chk-sca\", \"assignedTeams\": [\"/CxServer\"]}' $'https://api-sca.checkmarx.net/api/uploads'  | jq -r .url)

echo "This is URL "$upload_url

#uploading the zip file of source code

curl -X PUT -k  --upload-file "mob.zip" $upload_url -H "Authorization: Bearer "$access_token

echo "uploading done"

# fetching the scan ID, the project ID can be got when the project

scan_id=$(curl -k -s -X $'POST' -H $'Host: api-sca.checkmarx.net' -H $'Accept-Encoding: gzip, deflate' -H $'Accept: application/json' -H $'Accept-Language: en' -H $'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36' -H $'Authorization: Bearer '$access_token -H $'Content-Type: application/json' --data-binary $'{\"project\" : {\"id\": \"'"$project_id"'","type": "upload", "handler": { "url":"'$upload_url'"}  }}'  $'https://api-sca.checkmarx.net/api/scans' | jq -r .id)

echo "This is scan-id "$scan_id

# quering for vunlnerability status


curl  -s -k -X $'GET' -H $'Host: api-sca.checkmarx.net' -H $'Accept-Encoding: gzip, deflate' -H $'Accept: */*' -H $'Accept-Language: en' -H $'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36' -H $'Authorization: Bearer '$access_token $'https://api-sca.checkmarx.net/risk-management/scans/'$scan_id

#waiting for scan to complete

sleep 60; 

echo "Scanning done"

#getting the vulns


curl -i -s -k -X $'GET'  -H $'Host: api-sca.checkmarx.net' -H $'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:99.0) Gecko/20100101 Firefox/99.0' -H $'Accept: application/json' -H $'Accept-Language: en-US,en;q=0.5' -H $'Accept-Encoding: gzip, deflate' -H $'Authorization: Bearer '$access_token -H $'Origin: https://swagger-open-api.herokuapp.com' -H $'Sec-Fetch-Dest: empty' -H $'Sec-Fetch-Mode: cors' -H $'Sec-Fetch-Site: cross-site' -H $'Te: trailers' $'https://api-sca.checkmarx.net/risk-management/risk-reports/'$scan_id'/vulnerabilities'
